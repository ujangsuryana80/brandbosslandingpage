<template>
  <footer class="vuero-footer">
    <div class="container">
      <div class="columns footer-body">
        <!-- Column -->
        <div class="column is-4">
          <div class="p-t-10 p-b-10">
            <img src="/@src/assets/illustrations/images/brandboss-logo.svg" />

            <div class="t-footer-content">Monitor and Improve Your Brand Performance</div>
            <div class="t-footer-content">022-2010606 / +6281310445410</div>
            <div class="t-footer-content">support@brandboss.id</div>

            <div class="item-medsos">
              <img src="/@src/assets/illustrations/images/medsos-ig.svg" class="img-medsos" />
              <img src="/@src/assets/illustrations/images/medsos-linkedin.svg" class="img-medsos" />
              <img src="/@src/assets/illustrations/images/medsos--twitter.svg" class="img-medsos" />
              <img src="/@src/assets/illustrations/images/medsos-youtube.svg" class="img-medsos" />
              <img src="/@src/assets/illustrations/images/medsos-facebook.svg" class="img-medsos" />
            </div>

              <img src="/@src/assets/illustrations/images/id.svg" class="img-medsos" />
              <img src="/@src/assets/illustrations/images/en.svg" class="img-medsos" />
              <img src="/@src/assets/illustrations/images/ar.svg" class="img-medsos" />

          </div>
        </div>
        <!-- Column -->
        <div class="column is-6 is-offset-2">
          <div class="columns is-flex-tablet-p">
            <!-- Column -->
            <div class="column" id="mobile">
            </div>
            <!-- Column -->
            <div class="column">
              <ul class="footer-column">
                <li class="t-footer-title">Product</li>
                <li class="column-item">
                  <a href="#"><span class="t-footer-content">Social Media Monitoring</span></a>
                </li>
                <li class="column-item">
                  <a href="#"><span class="t-footer-content">Social Media Analytics</span></a>
                </li>
                <li class="column-item">
                  <a href="#"><span class="t-footer-content">Social Media Listening</span></a>
                </li>
              </ul>
            </div>
            <!-- Column -->
            <div class="column">
              <ul class="footer-column">
                <li class="t-footer-title">Company</li>
                <li class="column-item">
                  <a href="#"><span class="t-footer-content">FAQ</span></a>
                </li>
                <li class="column-item">
                  <a href="#"><span class="t-footer-content">Pricing</span></a>
                </li>
                <li class="column-item">
                  <a href="#"><span class="t-footer-content">Contact Us</span></a>
                </li>
                <li class="column-item">
                  <a href="#"><span class="t-footer-content">Term of Services</span></a>
                </li>
                <li class="column-item">
                  <a href="#"><span class="t-footer-content">Privacy & Policy</span></a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="footer-copyright">
      </div>
    </div>
  </footer>
  <!-- /Simple light footer -->
</template>

<style lang="scss">
.vuero-footer {
  padding-bottom: 0 !important;
  padding-top: 4rem !important;
  background: var(--body-color);

  .footer-head {
    padding-bottom: 3rem;
    display: flex;
    align-items: center;
    justify-content: space-between;
    border-bottom: 1px solid var(--fade-grey-dark-4);

    .head-text {
      h3 {
        font-family: var(--font);
        font-size: 1.8rem;
        color: var(--dark-text);
      }

      p {
        font-size: 1.1rem;
        color: var(--light-text);
      }
    }

    .head-action {
      .buttons {
        .button {
          &.action-button {
            height: 36px;
            min-width: 140px;
          }

          &.chat-button {
            background: transparent;
            border: none;
            box-shadow: none;
            color: var(--primary);
            font-weight: 500;
          }
        }
      }
    }
  }

  .footer-body {
    padding-top: 3rem;

    .footer-column {
      padding-top: 20px;

      .column-header {
        font-family: var(--font-alt);
        text-transform: uppercase;
        color: var(--dark-text);
        font-size: 1rem;
        font-weight: 700;
        margin: 10px 0;
      }

      .column-item {
        padding-bottom: 10px;

        a {
          font-family: var(--font);
          color: var(--light-text);

          &:hover,
          &:focus {
            color: var(--primary);
          }
        }
      }
    }

    .social-links {
      display: flex;
      justify-content: flex-start;
      align-items: center;

      a {
        color: var(--light-text);
        margin: 0 5px;

        &:hover,
        &:focus {
          color: var(--primary);
        }
      }
    }

    .footer-description {
      color: var(--light-text);
    }

    .moto {
      color: var(--light-text);
    }

    .small-footer-logo {
      height: 36px;
    }
  }

  .footer-copyright {
    font-family: var(--font);
    color: var(--light-text);
    padding: 4rem 0 2rem;

    a {
      color: var(--light-text);

      &:hover,
      &:focus {
        color: var(--primary);
      }
    }
  }
}

.is-dark {
  .landing-page-wrapper {
    .vuero-footer {
      background: var(--landing-xxx-light-8);

      .footer-head {
        border-color: var(--landing-xxx-light-18);

        .head-text {
          h3 {
            color: var(--dark-dark-text);
          }

          p {
            font-size: 1.1rem;
            color: var(--light-text);
          }
        }

        .head-action {
          .buttons {
            .button {
              &.action-button {
                background: var(--primary);
                border-color: var(--primary);
              }

              &.chat-button {
                color: var(--primary);
                background: none !important;
              }
            }
          }
        }
      }

      .footer-body {
        .footer-column {
          .column-header {
            color: var(--dark-dark-text);
          }

          .column-item {
            a:hover {
              color: var(--primary);
            }
          }
        }

        .social-links {
          a:hover {
            color: var(--primary);
          }
        }
      }

      .footer-copyright {
        a {
          &:hover {
            color: var(--primary);
          }
        }
      }
    }
  }
}

@media (max-width: 767px) {
  .vuero-footer {
    .footer-head {
      flex-direction: column;
      text-align: center;

      .head-text {
        padding-bottom: 20px;
      }
    }

    .footer-body {
      padding-left: 20px;
      padding-right: 20px;
    }
  }
}

@media only screen and (min-width: 768px) and (max-width: 1024px) and (orientation: portrait) {
  .vuero-footer {
    .footer-head,
    .footer-body {
      padding-left: 20px;
      padding-right: 20px;
    }

    .footer-description {
      max-width: 400px;
    }
  }
}

@media only screen and (min-width: 768px) and (max-width: 1024px) and (orientation: landscape) {
  .vuero-footer {
    .footer-head,
    .footer-body {
      padding-left: 20px;
      padding-right: 20px;
    }
  }
}
</style>
